import { useRef } from 'react'
import styled from 'styled-components'

// CSSTransition
import { CSSTransition } from 'react-transition-group'


const FadeDiv = styled.div`
  &.fade-right-appear {
    opacity: 0;
    transform: translateX(-1rem);
  }
  
  &.fade-right-appear-active {
    opacity: 1;
    transform: translateX(0);
    transition: opacity 500ms, transform 500ms;
  }
`

export default function FadeRight({ children, inState = true, ...props }) {

  const nodeRef = useRef(null)

  return (
    <CSSTransition
      nodeRef={nodeRef} 
      in={inState}
      appear={true} 
      timeout={500}
      classNames="fade-right"
    >
      <FadeDiv className='fade' ref={nodeRef} {...props}>
        {children}
      </FadeDiv>
    </CSSTransition>
  )
}
