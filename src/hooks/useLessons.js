import { useContext, useCallback } from "react"
import { LessonsContext } from "../containers/LessonsProvider"

export default function useLessons() {
  const context = useContext(LessonsContext)

  if (context == undefined || (context.length != undefined && context.length == 0)) {
    throw new Error('useLessons() must be used inside a LessonsProvider')
  }

  const getLastDrill = useCallback(({ type }) => {
    let lastDrill = {date:'0'}
    context.lessons.forEach(lesson => {
      if (lesson.drill.type == type) {
        if (lesson.drill.date > lastDrill.date) {
          lastDrill = lesson.drill
        }
      }
    })
    return lastDrill
  }, [context.lessons])

  const getValues = useCallback((key, subKey='name') => {
    let values = []
    context.lessons.forEach(lesson => {
      if (Array.isArray(lesson[key])) {
        lesson[key].forEach(res => {
          if (!values.includes(res))
            values.push(res)        
        });
      } else if (typeof lesson[key] == 'object') {
        if (!values.includes(lesson[key][subKey]))
          values.push(lesson[key][subKey])        
      } else {
        if (!values.includes(lesson[key]))
          values.push(lesson[key])        
      }
    })
    return values
  }, [context.lessons])

  return { 
    ...context,
    getLastDrill,
    getValues
  }
}
