// containers
import FadeLeft from '../../containers/FadeLeft'
import FadeRight from '../../containers/FadeRight'
import ImgLoader from './ImgLoader'

// images
import ArrowRight from '../../assets/arrow_right.svg'

// styles
import './BackTitle.scss'

export default function BackTitle({ text, showButton, handleClick }) {
  return (
    <h2 className="title-top back-title" style={{ margin: 0 }}>
    {!showButton && <FadeRight>{text}</FadeRight>}
    {showButton && (
      <FadeLeft>
        <button 
          className='btn-reset' 
          onClick={handleClick}
        >
          <ImgLoader
            spinner={'1.4em'}
            src={ArrowRight} 
          />
        </button>
      </FadeLeft>
    )}
  </h2>
)
}