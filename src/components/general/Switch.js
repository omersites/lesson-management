import ReactSwitch from "react-switch"
import styled from "styled-components"

const Container = styled.div`
  display: flex;
  align-items: center;
  width: fit-content;
  margin: 0 auto;
`

const Span = styled.span`
  margin: 0 .5em;
`

export default function Switch({ state, label, offLabel, onLabel }) {
  const [checked, setChecked] = state

  const handleChange = (checked) => {
    setChecked(!checked)
  }

  const handleKeyPress = e => {
    if (e.key == 'Enter') {
      setChecked(prev => !prev)
    }
  }

  return (
    <Container>
      {offLabel && <Span>{offLabel}</Span>}
      <ReactSwitch
        checked={!checked}
        onChange={handleChange}
        onKeyPress={handleKeyPress}
        // offColor="#b6d1ff"
        // offHandleColor="#6691e1"
        offColor="#888"
        offHandleColor="#fff"
        onColor="#888"
        onHandleColor="#fff"
        handleDiameter={30}
        uncheckedIcon={false}
        checkedIcon={false}
        boxShadow="0px 1px 5px rgba(0, 0, 0, 0.2)"
        activeBoxShadow="0px 0px 1px 10px rgba(255, 255, 255, 0.4)"
        height={20}
        width={50}
        className="react-switch"
        id="material-switch"
        aria-label={label}
      />
      {onLabel && <Span>{onLabel}</Span>}
    </Container>
  )
}