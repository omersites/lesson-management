// styles
import './IncrementBtn.scss'

// images
import LoadImg from '../../assets/load.svg';


export default function IncrementBtn({ onClick, sliceInfo, isFull }) {

  return (
    <div style={{ paddingBottom: '.5em'}}>
      <div className="slice-count">מראה {sliceInfo} לקחים</div>
      {!isFull && (
        <button onClick={onClick} className="increment-btn">
          <span>טען עוד</span>
          <img src={LoadImg} />
        </button>
      )}
    </div>
  )
}
