import { useEffect, useRef, useState, useCallback } from 'react'

// styles
import './FilterBar.scss'

// match json keys to names
const jsonNames = {
  id: 'סימוכין',
  drill: 'תרגיל/מבצע',
  debrief: 'תחקיר',
  subject: 'נושא',
  subjectPlus: 'תיאור נושא',
  stone: 'אבן בניין הכוח',
  mission: 'משימה',
  date: 'תג"ב',
  status: 'סטטוס',
  statusPlus: 'הערות סטטוס',
  responsibility: 'אחריות'
}

export default function FilterBar({ arr, filterParams, queryObj, setQueryObj }) {

  const formRef = useRef(null);
  const [filterOptions, setFilterOptions] = useState({})

  // update available filter options on data change
  useEffect(() => {
    const filterOptions = {}
    arr.forEach(item => {
      filterParams.forEach(param => {
        if (!filterOptions[param]) filterOptions[param] = []
        // iterate options seperated by commas
        item[param].split(',').forEach(item => {
          item = item.trim()
          if (!filterOptions[param].includes(item))
            filterOptions[param].push(item)
        });
      })
    })
    setFilterOptions(filterOptions)
  }, [arr, filterParams])

  // handle form submit
  const submitForm = () => {
    const form = formRef.current
    const selects = form.querySelectorAll('select')
    let queryObj = {}
    selects.forEach(sel => {
      if (sel.value) {
        queryObj[sel.name] = sel.value
      }
    })
    Promise.resolve().then(()=> {
      setQueryObj(queryObj)
    })
  }
  
  // handle x click 
  const handleClick = (e) => {
    if (e.target.className.includes('filled')) {
      let clone = {...queryObj}
      delete clone[e.target.children[0].name]
      setQueryObj(clone)
    }
  }

  // handle enter click
  const handleKeyPress = e => {
    if (e.key == 'Enter') {
      if (e.target.parentNode.className.includes('filled')) {
        e.preventDefault()
        let clone = {...queryObj}
        delete clone[e.target.parentNode.children[0].name]
        setQueryObj(clone)
      }
    }
  }

  return (
    <div className="filter-bar" ref={formRef}>
        {Object.keys(filterOptions).map(key => (
          <div 
            key={key} 
            className={queryObj[key] ? 'select filled' : 'select'} 
            onClick={handleClick}
            onKeyPress={handleKeyPress}
          >
            <select 
              name={key}
              value={queryObj[key] || ''}
              onChange={submitForm}
            >
              <option value="" hidden>בחר {jsonNames[key]}</option>
              {filterOptions[key].map(option => (
                <option value={option} key={option}>{option}</option>
              ))}
            </select >
          </div>
        ))}
    </div>
  )
}
