// router
import { NavLink } from "react-router-dom";

// styles
import "./NavBar.scss";

//images
import Home from "../../assets/home2.svg";
import Table from "../../assets/table.svg";
import Stats from "../../assets/pie.svg";
import Me from "../../assets/person.svg";
import Filter from "../../assets/filter.svg";

// buttons
const buttons = [
  { id: 1, text: "עמוד הבית", to: "/home", src: Home },
  { id: 2, text: "טבלת לקחים", to: "/table", src: Table },
  { id: 3, text: "תמונת מצב לקחים", to: "/stats", src: Stats },
  { id: 4, text: "הלקחים שלי", to: "/me", src: Me },
  { id: 5, text: "מיון אירוע/שנה", to: "/filter", src: Filter },
];

export default function NavBar() {
  return (
    <nav>
      {buttons.map((btn) => (
        <NavLink 
          key={btn.id} 
          to={btn.to} 
          className="button"
          tool-bottom={btn.text}
        >
          <div className="con">
            <img src={btn.src} />
          </div>
        </NavLink>
      ))}
    </nav>
  );
}
