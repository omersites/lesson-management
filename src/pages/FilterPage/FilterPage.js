import styled from "styled-components"

// custom hooks
import useUrlParam from '../../hooks/useUrlParam'
import useLessons from '../../hooks/useLessons'

// containers
import FadeDown from "../../containers/FadeDown"

// components
import Picker from "../../components/general/Picker"
import BackTitle from "../../components/general/BackTitle"
import FilterPage_stats from "./FilterPage_stats"

// styles
const FilterPageCon = styled.div`
  width: 100%;
`

const filters = {
  ['אירוע']: {
    key: 'drill',
    subKey: 'name'
  },
  ['שנה']: {
    key: 'date',
    subKey: 'year'
  },
}

export default function FilterPage() {

  const { getValues } = useLessons()

  const [filterType, setFilterType] = useUrlParam('type')
  const [filter, setFilter] = useUrlParam('filter')

  const handleBack = () => {
    if (filter) {
      setFilter('')
    } else if (filterType) {
      setFilterType('')
    }
  }

  return (
    <FilterPageCon>
      <BackTitle 
        text="מיון אירוע\שנה"
        showButton={filterType || filter}
        handleClick={handleBack}
      />
      {!filterType && !filter && (
        <FadeDown>
          <Picker 
            options={['אירוע', 'שנה']}
            handleClick={option => setFilterType(option)}
          />
        </FadeDown>
      )}
      {filterType && !filter && (   
        <FadeDown>
          <Picker 
            options={getValues(filters[filterType].key, filters[filterType].subKey)} 
            handleClick={option => setFilter(option)}
          />
        </FadeDown>
      )}
      {filterType && filter && (
        <FadeDown>
          <FilterPage_stats 
            mainKey={filters[filterType].key}
            subKey={filters[filterType].subKey}
            value={filter} 
          />
        </FadeDown>
      )}
    </FilterPageCon>
  )
}
