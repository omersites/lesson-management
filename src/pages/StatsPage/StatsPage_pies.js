// custom hooks
import useLessons from '../../hooks/useLessons'
import useStatusStats from '../../hooks/useStatusStats'

// components
import Pies from '../../components/lesson_stats/Pies'

export default function StatsPage_pies(props) {

  const { lessons, getLastDrill } = useLessons()

  // first pie
  const { valuesPercentage: pieData_1 } = useStatusStats(lessons, {})

  // second pie
  const lastdrill_1 = getLastDrill({ type: 'תרגיל' })
  const { valuesPercentage: pieData_2 } = useStatusStats(lessons, { drill: lastdrill_1 })

  // third pie
  const lastdrill_2 = getLastDrill({ type: 'מבצע' })
  const { valuesPercentage: pieData_3 } = useStatusStats(lessons, { drill: lastdrill_2 })

  
  return (
    <Pies pies={[
      { title: {normal: 'התפלגות סטטוס יישום של ', bold: 'כלל הלקחים במערכת'}, greenValue: pieData_1.green, yellowValue: pieData_1.yellow, redValue: pieData_1.red },
      { title: { normal: 'התפלגות סטטוס יישום ', bold: `כלל הלקחים מהתרגיל האחרון (${lastdrill_1.name})`}, greenValue: pieData_2.green, yellowValue: pieData_2.yellow, redValue: pieData_2.red },
      { title: { normal: 'התפלגות סטטוס יישום ', bold: `כלל הלקחים מהמבצע האחרון (${lastdrill_2.name})`}, greenValue: pieData_3.green, yellowValue: pieData_3.yellow, redValue: pieData_3.red },
    ]} style={{
      margin: '0 auto',
      width: '100%',
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'center',
      alignItems: 'center',
      gap: '5vmax'
    }} {...props}/>
  )
}
