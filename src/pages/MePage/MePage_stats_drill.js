import { useState } from 'react'

// custom hooks
import useLessons from '../../hooks/useLessons'
import useStatusStats from '../../hooks/useStatusStats'

// components
import StyledContainer from "../../components/general/StyledContainer"
import FilterBar from '../../components/lesson_table/FilterBar'
import Pie from '../../components/lesson_stats/Pie'
import Graph from '../../components/lesson_stats/Graph'

// styles
import './MePage_stats_drill.scss'

export default function MePage_stats_drill({ picked }) {

  const { formattedLessons } = useLessons()
  const [queryObj, setQueryObj] = useState({})
  
  // pie
  const { valuesPercentage: pieData } = useStatusStats(formattedLessons, {...queryObj, responsibility: picked})
  const pie = { 
		title: {
			normal: 'התפלגות סטטוס יישום של כלל הלקחים של המחלקה ', 
			bold: `מ${queryObj.drill || 'כלל האירועים'}`
		}, 
    greenValue: pieData.green, 
    yellowValue: pieData.yellow, 
    redValue: pieData.red 
  }

  // graph
  const subjectsStatusStats = useStatusStats(formattedLessons, {...queryObj, responsibility: picked}, 'subject')
  const graphData = subjectsStatusStats.reduce((arr, item) => ([ ...arr, {name: item.key, value: item.valuesPercentage.green.toFixed(1)}]), [])
  const graph = {
    title: {
      normal: `התפלגות יישום הלקחים של המחלקה מ${queryObj.drill || 'כלל האירועים'} `, 
      bold: 'ע"פ נושאים'
    },
    data: graphData
  }

  return (
    <StyledContainer className="me_page-stats-drill">
      <FilterBar 
        arr={formattedLessons}  
        queryObj={queryObj}
        setQueryObj={setQueryObj}
        filterParams={['drill']}
      /> 
      <h3 className="title-top">{queryObj.drill || 'כלל האירועים'}</h3>
      <Pie pieData={pie} />
      <Graph graph={graph} style={{
        width: '35em',
        height: '25em',
        maxWidth: '100%',        
      }}/>
    </StyledContainer>
  )
}
