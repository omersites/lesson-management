// containers
import FadeLeft from '../../containers/FadeLeft'
import FadeRight from '../../containers/FadeRight'
import ImgLoader from '../../components/general/ImgLoader'

// images
import ArrowRight from '../../assets/arrow_right.svg'

// styles
import './MePage_title.scss'

export default function MePage_title({ picked, setPicked }) {
  return (
    <h2 className="title-top me_page-title" style={{ margin: 0 }}>
    {!picked && <FadeRight>הלקחים שלי</FadeRight>}
    {picked && (
      <FadeLeft>
        <button 
          className='btn-reset' 
          onClick={() => setPicked('')}
        >
          <ImgLoader
            spinner={'1.4em'}
            src={ArrowRight} 
          />
        </button>
      </FadeLeft>
    )}
  </h2>
)
}