// containers
import DelayRender from '../../containers/DelayRender'

// components
import MePage_stats_pie from './MePage_stats_pie'
import MePage_stats_drill from './MePage_stats_drill'
import MePage_stats_table from './MePage_stats_table'

export default function MePage_stats({ picked }) {

  return (
    <div className="me_page-stats">
      <h3 className="title-top bright">{picked}</h3>
      <DelayRender>
        <MePage_stats_pie picked={picked} />
        <MePage_stats_drill picked={picked} />
        <div style={{padding: '0 1em'}}>
          <MePage_stats_table picked={picked} />
        </div>
      </DelayRender>
    </div>
  )
}