// custom hooks
import useLessons from '../../hooks/useLessons'
import useStatusStats from '../../hooks/useStatusStats'

// components
import Pies from '../../components/lesson_stats/Pies'

export default function MePage_stats_pie({ picked }) {

  const { lessons } = useLessons()

  const { valuesPercentage: pieData_1 } = useStatusStats(lessons, {responsibility: picked})

  const pie = { 
		title: {
			normal: 'התפלגות סטטוס יישום של ', 
			bold: `כלל הלקחים של המחלקה באחריות ${picked}`
		}, 
    greenValue: pieData_1.green, 
    yellowValue: pieData_1.yellow, 
    redValue: pieData_1.red 
  }

  return (
    <Pies pies={[pie]} style={{
      display: 'flex',
      justifyContent: 'center'
    }}/>
  )
}